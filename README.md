# Raspberry Pi - setup

In order to install and start the software required for proper Raspberry Pi work below steps have to be executed:

### *APPLICATION STARTUP*
1. Download the repository responsible for setting Raspberry Pi up:
```
git clone https://gitlab.com/iot-aggregator/rpi-setup.git
```

2. Start script responsible for getting, installing and running required code with given parameters:
- *g* - Raspberry Pi GUID given by Integration Service while [registering](https://zajaczkowski.dev/#/configs) device in system (*Add RaspberryPi* button),
- *m* - time interval (in seconds) between sending measurements from all registered sensors to the [Grafana](https://zajaczkowski.dev/#/),
- *p* - time interval (in seconds) between sending taken photographs to the [Gallery](https://zajaczkowski.dev/#/gallery)
```
cd rpi-setup
source run.sh -g 70ed2012-fed3-4df3-9f0b-9c130b216100 -m 300 -p 14400
```

After successful application startup the below information should be visible after 10 - 15 seconds:

```
-------------------------------------
|                                   |
|  RASPBERRY PI IS UP AND RUNNING.  |
|    SERVICE STARTED CORRECTLY.     |
|                                   |
-------------------------------------
```

### *APPLICATION EXIT*
When you want to stop the application simply type:
```
source exit.sh
```

This command will shut down all programs and processes associated with the application. After successful application exit the below information should be visible:
```
------------------------------------
|                                  |
|    SERVICE EXITED CORRECTLY.     |
|                                  |
------------------------------------
```
