#!/bin/bash

# Prepare setup 
echo "Preparing Python virtual environment ... "
python3 -m venv data-collector-venv
cd data-collector-venv
git clone https://gitlab.com/iot-aggregator/data-collector.git
echo "Activating Python virtual environment ... "
source bin/activate
cd data-collector
echo "Installling required dependencies ... "
python3 -m pip install -r requirements.txt

# Parse command line arguments
while getopts g:m:p: flag
do
    case "${flag}" in
        g) guid=${OPTARG};;
        m) mesurements_interval=${OPTARG};;
        p) photographs_interval=${OPTARG};;
    esac
done

# Start data collector
echo "Starting Python main application ... "
touch ../../main.pid
nohup python3 main.py $guid $mesurements_interval $photographs_interval &
echo "$!" > ../../main.pid

# Start Flask server
echo "Starting Python Flask service ... "
touch ../../flask_server.pid
nohup python3 flask_main.py &
echo "$!" > ../../flask_server.pid

# Go to the main dir
cd ../..
sleep 5

# Inform about successful application startup
echo "-------------------------------------"
echo "|                                   |"
echo "|  RASPBERRY PI IS UP AND RUNNING.  |"
echo "|    SERVICE STARTED CORRECTLY.     |"
echo "|                                   |"
echo "-------------------------------------"
