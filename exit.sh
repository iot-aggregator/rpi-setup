#!/bin/bash

# Exit virtual environment
echo "Exiting Python virtual environment ... "
deactivate

# Get python program PIDs
_pid_flask=$(<flask_server.pid)
_pid_main=$(<main.pid)

# Terminate processes
echo "Terminating Python programs ... "
kill -9 $_pid_flask
kill -15 $_pid_main

# Remove PID files
rm -f flask_server.pid
rm -f main.pid

# Remove Python virtual environment
rm -rf data-collector-venv

# Inform about successful application exit
echo "------------------------------------"
echo "|                                  |"
echo "|    SERVICE EXITED CORRECTLY.     |"
echo "|                                  |"
echo "------------------------------------"
